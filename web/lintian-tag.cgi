#!/usr/bin/ruby

$:.unshift('../rlibs')
require 'udd-db'
require 'cgi'
require 'pp'

puts "Content-type: text/html; charset=utf-8\n\n"

now = Time::now
cgi = CGI::new

if cgi.has_key?('tag')
  tag = cgi.params['tag'][0]
else
  tag = nil
end

if tag.nil?
  puts "Must provide a tag as parameter."
  exit(0)
end

if tag !~ /^[a-z0-9_.-]+$/i
  puts "Invalid characters in tag name"
  exit(0)
end

q = <<-EOF
select source, version, package, package_version, architectures::text[] architectures, tag_type, tag, information, lintian_version, count
FROM lintian_results_agg
WHERE tag = '#{tag}'
ORDER BY 1,2,3,4,6,7
EOF
DB = Sequel.connect(UDD_GUEST)
res = DB[q].map { |r| r.to_h }
res.each do |e|
  e[:architectures] = e[:architectures].gsub('NULL', 'source').gsub(/^\{(.*)\}$/, '\1').split(',').uniq.sort
end

puts <<-EOF
<!DOCTYPE html><html><head>
<link href="/css/debian.css" rel="stylesheet" type="text/css">
<link href="/css/udd.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="/css/jquery.dataTables.1.13.6.css"/>
<title>Lintian tag information: #{tag}</title>
</head>
<body>
<h1>Lintian tag information: #{tag}</h1>

<table class="buglist table table-bordered table-hover display compact" id="lintian">
<thead>
<tr>
    <th>source</th>
    <th>version</th>
    <th>binary</th>
    <th>tag type</th>
    <th>tag</th>
    <th>information</th>
    <th><span title="tags affecting binary packages are counted once per architecture">count</th>
</tr>
</thead>
<tbody>
EOF
res.each do |t|
  if t[:package]
    bin = "<span title=\"#{t[:architectures].join(',')}\">#{t[:package]}/#{t[:package_version]}</span>"
  else
    bin = ""
  end
  puts <<-EOF
    <tr>
      <td class="nowrap"><a href="https://tracker.debian.org/#{t[:source]}">#{t[:source]}</a></td>
      <td class="nowrap">#{t[:version]}</td>
      <td class="nowrap">#{bin}</td>
      <td class="nowrap">#{t[:tag_type]}</td>
      <td class="nowrap">#{t[:tag]}</td>
      <td class="nowrap">#{t[:information]}</td>
      <td class="nowrap">#{t[:count]}</td>
    </tr>
  EOF
end
puts <<-EOF
</tbody>
</table>
</body>
</html>
EOF

