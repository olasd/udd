User-agent: *
Disallow: /dmd
Disallow: /dmd.cgi
Disallow: /lintian
Disallow: /lintian.cgi
Disallow: /bugs
Disallow: /bugs.cgi
